RakeOE
======

Bei RakeOE handelt es sich um eine Buildumgebung welche es einem auf einfache Art und Weise ermöglicht den von Ihnen erstellten C-Code in den Machinen Code für den [Controller](http://www.st.com/web/en/catalog/mmc/FM141/SC1169/SS1574/LN1823/PF259605) umwandelt.


Installation
------------

Nach einer erfolgreichen Ruby Installation und hinzufügen von Ruby zu Ihrem [System Pfad](http://www.java.com/de/download/help/path.xml) kann RakeOE wie auf der [RakeOE Projektseite](https://github.com/rakeoe/rakeoe/wiki/Installation) beschrieben installiert werden.

